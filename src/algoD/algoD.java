package algoD;

import algoD.interpreter.RUNTIME;
import algoD.interpreter.translate.Translator;
import dcc.DCoutputH;
import dcc.m.textFile.READER;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author dusakus
 */
public class algoD {

    public static void main(String[] args) {
        StData.log = new DCoutputH(true, System.getProperty("user.dir") + "/stuDI0/algo-D/Log.dcl");
        StData.RT = new RUNTIME();
        int e = StData.RT.INIT();
        if(e != 0){
            System.exit(e);
        }
        //CHECK(dcc.frame.minis.getfile.one(StData.log, "gimme language file"));
        Translator.LoadFile(dcc.frame.minis.getfile.one(StData.log, "gimme language file"));
        StData.log.println(Translator.translate("fałsz", "BOOLEAN"));
        StData.gui = new interGUI();
        StData.gui.main(new String[0]);

    }

    public static void CHECK(File f) {
        READER read = null;
        try {
            read = new READER(f);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(algoD.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (true) {
            try {
                System.out.println(read.nextLine());
            } catch (IOException ex) {
                Logger.getLogger(algoD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
