/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package algoD.helpers;

import algoD.StData;
import dcc.sify.MultiString;

/**
 * @author dusakus
 */
public class ERROR {
    public String where = "Not known";
    public String text = "Unknown error";
    public int code = 0;
    public MultiString moarText = new MultiString("EMPTY");
    
    public void writeOut(){
        StData.log.longMode("An "+code+" error occured");
        StData.log.println("where: "+where);
        StData.log.println("what: "+text);
        StData.log.print(moarText);
        StData.log.longMode("End of error mesage");
                
    }
}
