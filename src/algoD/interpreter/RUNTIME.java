package algoD.interpreter;

import algoD.interpreter.parts.Storage.STORAGE;
import algoD.interpreter.translate.Translator;
import algoD.StData;
import algoD.helpers.ERROR;
import dcc.sify.MultiString;
import dcc.sify.SimString;

/**
 * @author dusakus
 */
public class RUNTIME {

    public STORAGE memory;
    AlgDecoder decoder;
    CODE program;

    public int INIT() {
        memory = new STORAGE();
        int a = memory.INIT();
        if (a != 0) {
            return a;
        }
        a = Translator.INIT();
        if (a != 0) {
            return a;
        }
        decoder = new AlgDecoder();
        a = decoder.INIT();
        if (a != 0) {
            return a;
        }

        return 0;
    }

    public int RUN(CODE prog) {
        program = prog;
        if (prog.isEmpty()) {
            return 801;
        }
        

        return 0;
    }
    private int process(String line){
        int ToDo = decoder.decode(SimString.rmAfter(line, '-'));
        switch(ToDo){
            case 1:
            case 2:
            case 3:
            case 4:
            case 0:
                ERROR e = new ERROR();
                e.code = 911;
                e.where = "current code at line "+program.getCurrent();
                e.text = "Failed to process line >>> "+line;
                e.moarText = new MultiString("The word "+SimString.rmAfter(line, '-')+" might be misspelled");
                e.moarText.addL("no translations were found, you might want to check");
                e.moarText.addL("for updates, or loading of your language file");
                return 911;
        }
        return 0;
    }
}
