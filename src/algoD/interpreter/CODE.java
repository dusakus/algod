/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoD.interpreter;

import dcc.DCoutputH;
import java.util.ArrayList;
import algoD.StData;

/**
 * @author dusakus
 */
public class CODE {

    private ArrayList<String> text;
    private ArrayList<String> args;

    public String name = "null";

    private int current = 0;
    private int currentArg = 0;

    public CODE(String a) {
        name = a;
        text = new ArrayList<String>();
        args = new ArrayList<String>();
    }

    public void addLine(String line) {
        text.add(line);
    }
    
    public void addArg(String line) {
        args.add(line);
    }

    public String getNextLine() {
        if (!text.isEmpty() && text.size() >= current) {
            return text.get(current++);
        } else {
            return "END";
        }
    }

    public String getNextArg() {
        if (!args.isEmpty() && args.size() >= currentArg) {
            return args.get(currentArg++);
        } else {
            return "END";
        }
    }

    public void reset() {
        current = 0;
        currentArg = 0;
    }

    public boolean isEmpty() {
        return text.isEmpty();
    }

    public String getLine(int index) {
        return text.get(index + 1);
    }

    public int getCurrent() {
        return current + 1;
    }

    public void PRINT(DCoutputH output) {
        if (!text.isEmpty()) {
            output.longMode("Started Program");
            output.println("Name: " + name);
            output.println("Code lenght: " + (text.size()));
            output.println("Args lenght: " + (args.size()));
            output.println("Current line (" + (current) + "): " + text.get(current));

            output.println("Args:");
            for (String a : args) {
                output.println(a);
            }
            output.println("--------------------------------------------");
            output.println("Code:");
            for (String a : text) {
                output.println(a);
            }
            output.longMode("END");
        } else {
            output.println("No code in there!!!");
        }
    }

}
