/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoD.interpreter.parts.Storage;

import algoD.interpreter.translate.Translator;

/**
 * @author dusakus
 */
public class sType_BOOLEAN extends sType_BASE {

    public final int typeI = 1;
    public final String typeS = "BOOLEAN";
    private boolean value = false;

    public boolean get() {
        return value;
    }

    @Override
    public int parse() {
        switch (valueS.trim()) {
            case ("true"):
                value = true;
                return 0;
            case ("false"):
                value = false;
                return 0;
            case ("T"):
                value = true;
                return 0;
            case ("F"):
                value = false;
                return 0;
        }
        String t = Translator.translate(valueS, "BOOLEAN");
        if (!t.equals("INVALID")) {
            valueS = t;
            parse();
            return 0;
        } else {
            return 922;
        }
    }
}
