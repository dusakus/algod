/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoD.interpreter.parts.Storage;

import algoD.interpreter.translate.Translator;

/**
 * @author dusakus
 */
public class sType_CHARACTER extends sType_BASE {

    public final String typeS = "CHARACTER";
    public final int typeI = 2;
    public char value;

    public char get() {
        return value;
    }

    @Override
    public int parse() {

        if (valueS.trim().length() == 1) {
            value = valueS.charAt(0);
            return 0;
        } else {
            return 922;
        }
    }
}
