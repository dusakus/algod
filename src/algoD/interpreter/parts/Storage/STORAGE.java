/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoD.interpreter.parts.Storage;

import algoD.interpreter.translate.Translator;
import java.util.ArrayList;

/**
 * @author dusakus
 */
public class STORAGE {

    private ArrayList<sType_BASE> objects;

    public int DO(String line) {
        return 0;
    }

    public int add(String t, String n, String v) {
        int T = 0;

        switch (t.toUpperCase()) {
            case "BOOLEAN":
                T = 1;
            case "CHARACTER":
                T = 2;
            case "NUMBER":
                T = 3;
            case "STRING":
                T = 4;
        }
        if (T == 0) {
            return add(Translator.translate(t, "STYPE"), n, v);
        }
        return add(T, n, v);
    }

    public int add(int t, String n, String v) {
        switch (t) {
            case 0:
                return 923;
            case 1:

        }

        return 0;
    }

    public sType_BASE get(String what) {
        for (sType_BASE o : objects) {
            if (o.name.equals(what)) {
                return o;
            } else {
            }
        }
        return null;
    }

    public void CLEAN() {
        objects.clear();
    }

    public int INIT() {
        objects = new ArrayList<sType_BASE>();
        return 0;
    }
}
