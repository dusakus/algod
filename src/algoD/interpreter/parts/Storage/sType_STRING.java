/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoD.interpreter.parts.Storage;

/**
 * @author dusakus
 */
public class sType_STRING extends sType_BASE {

    public final String typeS = "STRING";
    public final int typeI = 4;

    public String get() {
        return valueS;
    }

    @Override
    public int parse() {
        return 0;
    }

    public void add(String s) {
        valueS = valueS + s;
    }
}
