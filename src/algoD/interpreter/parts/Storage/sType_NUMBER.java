/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoD.interpreter.parts.Storage;

/**
 * @author dusakus
 */
public class sType_NUMBER extends sType_BASE {

    public final String typeS = "NUMBER";
    public final int typeI = 3;
    public int value;

    public int get() {
        return value;
    }

    @Override
    public int parse() {
        try {
            value = Integer.parseInt(valueS.trim());
            return 0;
        } catch (NumberFormatException e) {
            return 922;
        }
    }
}
