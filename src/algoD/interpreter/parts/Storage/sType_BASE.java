/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoD.interpreter.parts.Storage;

/**
 * @author dusakus
 */
public class sType_BASE {

    public final String typeS = "X";
    public final int typeI = 0;
    String valueS = null;
    String name = "Unnamed " + System.currentTimeMillis();

    public void setName(String in) {
        name = in;
    }

    public String getName() {
        return name;
    }

    public String getTypeS() {
        return typeS;
    }

    public int getTypeI() {
        return typeI;
    }

    public int set(String v) {
        valueS = v;
        return parse();
    }

    public String getS() {
        return valueS;
    }

    int parse() {
        return 0;
    }
}
